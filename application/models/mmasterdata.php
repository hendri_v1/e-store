<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mmasterdata extends CI_Model {
	
	//category put here
	var $category_id='';
	var $category_name='';
	var $category_detail='';
	
	//product put here
	var $product_id='';
	var $product_name='';
	var $product_information='';
	var $product_status='';
	var $product_specification='';
	
	//supplier
	var $supplier_id='';
	var $supplier_name='';
	var $supplier_address='';
	var $supplier_phone='';
	var $supplier_cp='';
	var $supplier_city='';
	var $supplier_email='';
	var $supplier_website='';
	var $supplier_account='';
	
	//category function
	function get_all_category($is_acc=false)
	{
		$this->db->order_by('category_name');
		if($is_acc==true)
			$this->db->where('category_detail','x');
		$query=$this->db->get('category');
		return $query->result();	
	}
	
	function add_new_category()
	{
		$this->db->set('category_name',$this->category_name);
		$this->db->set('category_detail',$this->category_detail);
		$this->db->insert('category');	
	}
	
	//supplier function
	function get_all_supplier()
	{
		$this->db->order_by('supplier_name');
		$query=$this->db->get('supplier');
		return $query->result();
	}
	
	function add_new_supplier()
	{
		$this->db->set('supplier_name',$this->supplier_name);
		$this->db->set('supplier_address',$this->supplier_address);
		$this->db->set('supplier_phone',$this->supplier_phone);
		$this->db->set('supplier_cp',$this->supplier_cp);
		$this->db->set('supplier_city',$this->supplier_city);
		$this->db->set('supplier_email',$this->supplier_email);
		$this->db->set('supplier_website',$this->supplier_website);
		$this->db->set('supplier_account',$this->supplier_account);
		$this->db->insert('supplier');	
	}
	
	//product function
	function get_all_product($is_acc=false)
	{
		$this->db->join('category','category.category_id=product.category_id');
		if($is_acc==true)
			$this->db->where('category.category_detail','x');
		$this->db->order_by('category.category_name');
		$this->db->order_by('product.product_name');
		$query=$this->db->get('product');
		return $query->result();	
	}
	
	function get_product_by_id($product_id)
	{
		$this->db->where('product_id',$product_id);
		$query=$this->db->get('product');
		return $query->row();	
	}
	
	function add_new_product()
	{
		$this->db->set('product_name',$this->product_name);
		$this->db->set('category_id',$this->category_id);
		$this->db->set('product_information',$this->product_information);
		$this->db->set('product_status',1);
		$this->db->set('product_specification',$this->product_specification);
		$this->db->insert('product');
	}
	
	function edit_product($product_id)
	{	
		$this->db->set('product_name',$this->product_name);
		$this->db->set('category_id',$this->category_id);
		$this->db->set('product_information',$this->product_information);
		$this->db->set('product_status',1);
		$this->db->set('product_specification',$this->product_specification);
		$this->db->where('product_id',$product_id);
			$this->db->update('product');
	}
	
	function check_product_exist($product_name)
	{
		$this->db->where('product_name',$product_name);
		$query=$this->db->get('product');
		return $query->num_rows;	
	}
	
	function load_spec($product_id)
	{
		$this->db->where('product_id',$product_id);
		$query=$this->db->get('product');
		return $query->row();	
	}
	
	function search_product($keywords)
	{
		$this->db->like('product_specification',$keywords);
		$this->db->join('category','category.category_id=product.category_id');
		$query=$this->db->get('product');
		return $query->result();	
	}
	
}