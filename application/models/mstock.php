<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mstock extends CI_Model {

	//for lcoation
	var $location_id='';
	var $location_name='';
	var $location_info='';
	var $location_contains='';
	
	//for items
	var $items_id='';
	var $items_code='';
	var $items_name='';
	var $items_unique='';
	var $product_id='';
	var $items_date_in='';
	var $items_date_out='';
	var $items_detail='';
	var $items_base_price='';
	var $items_sell_price='';
	var $items_source='';
	var $items_source_ref='';
	var $items_condition='';
	var $is_second='';
	var $is_modem='';
	var $items_qty='';
	
	//stock actitivy
	var $stock_activity_id='';
	var $stock_activity_date='';
	var $stock_activity_type='';
	var $stock_activity_log='';
	
	//stock check
	var $stock_check_id='';
	var $stock_check_date='';
	var $stock_check_day='';
	
	function get_location()
	{
		$query=$this->db->get('location');
		return $query->result();	
	}
	
	function add_new_location()
	{
		$this->db->set('location_name',$this->location_name);
		$this->db->set('location_info',$this->location_info);
		$this->db->insert('location');	
	}
	
	function add_activity($location_id,$items_id,$user_id,$stock_activity_type,$stock_activity_log)
	{
		$this->db->set('location_id',$location_id);
		$this->db->set('items_id',$items_id);
		$this->db->set('user_id',$user_id);
		$this->db->set('stock_activity_type',$stock_activity_type);
		$this->db->set('stock_activity_date',time());
		$this->db->set('stock_activity_log',$stock_activity_log);	
		$this->db->insert('stock_activity');
	}
	
	function increase_stock($location_id,$increasing)
	{
		$this->db->where('location_id',$location_id);
		$query=$this->db->get('location');
		$row=$query->row();
		$last_count=$row->location_contains;
		$new_count=$last_count+$increasing;
		$this->db->set('location_contains',$new_count);
		$this->db->set('location_last_log',time());
		$this->db->where('location_id',$location_id);
		$this->db->update('location');	
	}
	
	function check_code_available($items_code)
	{
		$this->db->where('items_code',$items_code);
		$this->db->where('items_status',0);
		$query=$this->db->get('items');
		$total=$query->num_rows;
		return $total;	
	}
	
	function add_new_items()
	{
		$this->db->set('items_code',$this->items_code);
		$this->db->set('items_name',$this->items_name);
		$this->db->set('items_detail',nl2br($this->items_detail));
		$this->db->set('product_id',$this->product_id);
		$this->db->set('location_id',$this->location_id);
		$this->db->set('items_base_price',$this->items_base_price);
		$this->db->set('items_date_in',time());
		$this->db->set('items_sell_price',$this->items_sell_price);
		$this->db->set('items_source',$this->items_source);
		$this->db->set('items_source_ref',$this->items_source_ref);
		$this->db->set('items_condition',$this->items_condition);
		$this->db->set('is_second',$this->is_second);
		$this->db->set('is_modem',$this->is_modem);
		$this->db->insert('items');
		
		return $this->db->insert_id();	
	}
	
	function add_new_items2()
	{
		$this->db->set('items_code',$this->items_code);
		$this->db->set('items_name',$this->items_name);
		$this->db->set('items_detail',nl2br($this->items_detail));
		$this->db->set('product_id',$this->product_id);
		$this->db->set('location_id',$this->location_id);
		$this->db->set('items_unique',1);
		$this->db->set('items_base_price',$this->items_base_price);
		$this->db->set('items_date_in',time());
		$this->db->set('items_sell_price',$this->items_sell_price);
		$this->db->set('items_source',$this->items_source);
		$this->db->set('items_source_ref',$this->items_source_ref);
		$this->db->set('items_condition',$this->items_condition);
		$this->db->set('is_second',$this->is_second);
		$this->db->set('items_qty',$this->items_qty);
		$this->db->insert('items');
		
		return $this->db->insert_id();	
	}
	
	function update_items($items_id)
	{
		$this->db->set('items_name',$this->items_name);
		$this->db->set('items_code',$this->items_code);
		$this->db->set('items_base_price',$this->items_base_price);
		$this->db->set('items_sell_price',$this->items_sell_price);
		$this->db->set('items_source',$this->items_source);
		$this->db->set('items_source_ref',$this->items_source_ref);
		$this->db->set('product_id',$this->product_id);
		$this->db->set('is_modem',$this->is_modem);
		$this->db->where('items_id',$items_id);
		$this->db->update('items');
	}
	
	function get_location_by_id($location_id)
	{
		$this->db->where('location_id',$location_id);
		$query=$this->db->get('location');
		return $query->row();	
	}
	
	function get_all_items($selection="all")
	{
		$this->db->where('items_status',0);
		if($selection=="all")
		{
				
		}
		elseif($selection=="new")
		{
			$this->db->where('is_second',0);
			$this->db->where('items_unique',0);
			$this->db->where('is_modem',0);
		}
		elseif($selection=="2nd")
		{
			$this->db->where('is_second',1);
			$this->db->where('items_unique',0);	
			$this->db->where('is_modem',0);
		}
		elseif($selection="modem")
		{
			$this->db->where('is_modem',1);	
		}
		$query=$this->db->get('items');
		//echo $this->db->last_query();
		return $query->num_rows;	
	}
	
	function get_product_by_id($product_id)
	{
		$this->db->where('product_id',$product_id);
		$query=$this->db->get('product');
		return $query->row();	
	}
	
	function get_items_by_location($location_id,$isgrouped="yes",$is_acc=false)
	{
		
		
		if($isgrouped=="yes")
		{
			
			$this->db->select('product.product_name, COUNT(items.items_id) as total_items, category.category_name, items.product_id');
			$this->db->group_by('items.product_id');
		}
		$this->db->where('items.location_id',$location_id);
		$this->db->where('items.items_status',0);
		if($is_acc==true)
		{
			$this->db->where('items.items_unique',1);	
		}
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$query=$this->db->get('items');
		return $query->result();
	}

	function get_items_by_location2()
	{
		$this->db->select('product.product_name, COUNT(items.items_id) as total_items, category.category_name, items.product_id');
		$this->db->group_by('items.product_id');
		$this->db->where('items.location_id',1);
		//$this->db->where('items.items_status',0);
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$query=$this->db->get('items');
		return $query->result();
	}
	
	function get_items_by_product($product_id,$location_id="all",$stock_only="no")
	{
		$this->db->join('supplier','supplier.supplier_id=items.items_source');
		$this->db->where('product_id',$product_id);
		$this->db->order_by('items_status');
		if($location_id<>"all")
			$this->db->where('location_id',$location_id);
		if($stock_only<>"no")
		 	$this->db->where('items_status',0);
		$query=$this->db->get('items');
		return $query->result();	
	}
	
	function get_total_items_by_product($product_id)
	{
		$this->db->where('product_id',$product_id);
		$this->db->where('items_status',0);
		
		$query=$this->db->get('items');
		return $query->num_rows();
	}
	
	function get_items_by_id($items_id)
	{
		$this->db->where('items_id',$items_id);
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->join('location','location.location_id=items.location_id');
		$this->db->join('supplier','supplier.supplier_id=items.items_source');
		$query=$this->db->get('items');
		return $query->row();
	}
	
	function get_items_by_status($items_status)
	{
		$this->db->where('items_status',$items_status);
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->join('location','location.location_id=items.location_id');
		$this->db->join('supplier','items.items_source=supplier.supplier_id');
		$query=$this->db->get('items');
		return $query->result();
	}
	
	function get_items_by_code($items_code)
	{
		$this->db->where('items.items_code',$items_code);
		$this->db->where('items.items_status',0);
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->join('location','location.location_id=items.location_id');
		$query=$this->db->get('items');
		return $query->row();
	}
	
	function get_items_by_code_all($items_code)
	{
		$this->db->where('items.items_code',$items_code);
		$this->db->where('items.items_status',1);
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->join('location','location.location_id=items.location_id');
		$query=$this->db->get('items');
		return $query->row();
	}
	
	function get_activity_by_items($items_id)
	{
		$this->db->join('location','location.location_id=stock_activity.location_id');
		$this->db->join('user','user.user_id=stock_activity.user_id');
		$this->db->where('stock_activity.items_id',$items_id);
		$query=$this->db->get('stock_activity');
		return $query->result();
	}
		
	function get_items_for_json()
	{
		$this->db->select('items_code');
		$this->db->select('items_name');
		$this->db->select('items_source');
		$this->db->select('items_source_ref');
		$this->db->where('items_status',0);
		$query=$this->db->get('items');
		$kembali=array();
		$kembali['items_source']=array();
		$kembali['items_source_ref']=array();
		$kembali['items_code']=array();
		$kembali['items_name']=array();
		foreach($query->result() as $rows)
		{
			$kembali['items_code'][]=$rows->items_code;
			$kembali['items_name'][]=$rows->items_name;
			$kembali['items_source'][]=$rows->items_source;
			$kembali['items_source_ref'][]=$rows->items_source_ref;	
		}
		return $kembali;
	}
	
	function get_items_for_json_all()
	{
		$this->db->select('items_code');
		$this->db->select('items_name');
		$this->db->select('items_source');
		$this->db->select('items_source_ref');
		//$this->db->where('items_status',0);
		$query=$this->db->get('items');
		$kembali=array();
		$kembali['items_source']=array();
		$kembali['items_source_ref']=array();
		$kembali['items_code']=array();
		$kembali['items_name']=array();
		foreach($query->result() as $rows)
		{
			$kembali['items_code'][]=$rows->items_code;
			$kembali['items_name'][]=$rows->items_name;
			$kembali['items_source'][]=$rows->items_source;
			$kembali['items_source_ref'][]=$rows->items_source_ref;	
		}
		return $kembali;
	}
	
	function get_product_for_json()
	{
		$this->db->select('product.product_name');
		$this->db->select('product.product_id');
		$this->db->select('category.category_name');
		$this->db->join('category','category.category_id=product.category_id');
		$query=$this->db->get('product');
		$kembali=array();
		foreach($query->result() as $rows)
		{
			$kembali[]=$rows->product_id.' - '.$rows->category_name.' '.$rows->product_name;	
		}
		return json_encode($kembali);
	}
	
	function stock_check_in()
	{
		$this->db->where('items_id',$this->items_id);
		$this->db->where('stock_check_day',date('dmy'));
		$query=$this->db->get('stock_check');
		$total=$query->num_rows;
		
		if($total==0)
		{		
			$this->db->set('items_id',$this->items_id);
			$this->db->set('user_id',$this->session->userdata('user_id'));
			$this->db->set('stock_check_date',time());
			$this->db->set('stock_check_day',date('dmy'));
			$this->db->insert('stock_check');	
		}
	}
	
	function check_ada($items_code,$is_second,$is_modem)
	{
		$this->db->where('items_code',$items_code);
		$this->db->where('is_second',$is_second);
		$this->db->where('is_modem',$is_modem);
		$query=$this->db->get('items');
		return $query->num_rows;
	}
	
	function get_stock_check($user_id,$day,$is_second=0,$is_modem)
	{
		$this->db->join('items','items.items_id=stock_check.items_id');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('user','user.user_id=stock_check.user_id');
		//$this->db->where('stock_check.user_id',$user_id);
		$this->db->where('stock_check.stock_check_day',$day);
		if($is_second!=2)
			$this->db->where('items.is_second',$is_second);
		$this->db->where('items.is_modem',$is_modem);
		$this->db->order_by('stock_check_date','DESC');
		$query=$this->db->get('stock_check');
		return $query;	
	}
	
	function compare_stock($is_second=0,$is_modem=0)
	{
		if($is_second==2)
			$query='select items.items_id, items.items_code, items.items_name, items.is_second, product.product_name, category.category_name from items, product, category where product.product_id=items.product_id and category.category_id=product.category_id and items.items_status=0 and items.is_modem='.$is_modem.' and items.items_id not in (select items_id from stock_check where stock_check_day="'.date('dmy').'")';	
		else
			$query='select items.items_id, items.items_code, items.items_name, items.is_second, product.product_name, category.category_name from items, product, category where product.product_id=items.product_id and category.category_id=product.category_id and items.items_status=0 and items.is_second='.$is_second.' and items.is_modem='.$is_modem.' and items.items_id not in (select items_id from stock_check where stock_check_day="'.date('dmy').'")';
		$query=$this->db->query($query);
		return $query->result();	
	}
	
	function compare_stock_by_date($date,$is_second=0,$is_modem=0)
	{
		if($is_second==2)
			$query='select items.items_id, items.items_code, items.items_name, items.is_second, product.product_name, category.category_name from items, product, category where product.product_id=items.product_id and category.category_id=product.category_id and items.items_status=0 and items.is_modem='.$is_modem.' and items.items_id not in (select items_id from stock_check where stock_check_day="'.$date.'")';
		else
			$query='select items.items_id, items.items_code, items.items_name, items.is_second, product.product_name, category.category_name from items, product, category where product.product_id=items.product_id and category.category_id=product.category_id and items.items_status=0 and items.is_second='.$is_second.' and items.is_modem='.$is_modem.' and items.items_id not in (select items_id from stock_check where stock_check_day="'.$date.'")';
		$query=$this->db->query($query);
		return $query->result();	
	}
	
	function get_total_by_location($location_id,$items_unique=0)
	{
		$this->db->where('items_status',0);
		$this->db->where('location_id',$location_id);
		$this->db->where('items_unique',$items_unique);
		$query=$this->db->get('items');
		return $query->num_rows;	
	}

	function get_assets_by_location($location_id,$items_unique=0)
	{
		if($items_unique==0)
			$sql="select sum(items_base_price) as total_assets from items where items_status=0 and location_id=".$location_id;
		else
			$sql="select sum(items_base_price*items_qty) as total_assets from items where items_status=0 and location_id=".$location_id;
		$query=$this->db->query($sql);
		$row=$query->row();
		return $row->total_assets;
	}
	
	function get_total_by_product($product_id)
	{
		$this->db->where('items_status',0);
		$this->db->where('product_id',$product_id);
		$query=$this->db->get('items');
		return $query->num_rows;
	}
	
	function get_items_date_in_range($start_date,$end_date)
	{
		$this->db->select('count(items.items_source_ref) as total_items');
		$this->db->select('supplier.supplier_name');
		$this->db->select('items.items_source_ref');
		$this->db->select('items.items_date_in');
		$this->db->select('supplier.supplier_id');
		$this->db->join('supplier','supplier.supplier_id=items.items_source');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->group_by('items.items_source_ref');
		$this->db->order_by('supplier.supplier_name');
		$this->db->where('items.items_date_in >=',$start_date);
		$this->db->where('items.items_date_in <=',$end_date);
		$query=$this->db->get('items');
		return $query->result();
	}
	
	function get_items_date_in_range_per_sup($start_date,$end_date,$supplier_id)
	{
		$this->db->select('count(items.items_source_ref) as total_items');
		$this->db->select('supplier.supplier_name');
		$this->db->select('items.items_source_ref');
		$this->db->select('items.items_date_in');
		$this->db->select('supplier.supplier_id');
		$this->db->join('supplier','supplier.supplier_id=items.items_source');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->group_by('items.items_source_ref');
		$this->db->order_by('supplier.supplier_name');
		$this->db->where('items.items_date_in >=',$start_date);
		$this->db->where('items.items_date_in <=',$end_date);
		$this->db->where('supplier.supplier_id',$supplier_id);
		$query=$this->db->get('items');
		return $query->result();
	}

	function get_items_in_by_ref($items_source_ref)
	{
		$this->db->select('count(product.product_id) as total_product');
		$this->db->select('product.product_name');
		$this->db->select('items.items_name');
		$this->db->select('items.items_base_price');
		$this->db->select('category.category_name');
		$this->db->group_by('product.product_id');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->where('items.items_source_ref',$items_source_ref);
		$query=$this->db->get('items');
		return $query->result();
	}
	
	function get_items_by_price($items_base_price)
	{
		$lower=$items_base_price-100000;
		$higher=$items_base_price+100000;
		$this->db->where('items.items_base_price >=',$lower);
		$this->db->where('items.items_base_price <=',$higher);
		$this->db->where('items.items_status',0);
		$this->db->order_by('items.items_base_price','ASC');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->join('location','location.location_id=items.location_id');
		$this->db->join('supplier','supplier.supplier_id=items.items_source');
		$query=$this->db->get('items');
		return $query->result();
	}

	function change_items_status($items_id,$new_status)
	{
		$this->db->set('items_status',$new_status);
		$this->db->where('items_id',$items_id);
		$this->db->update('items');
	}
	
	function update_stock($items_id,$new_stock)
	{
		$this->db->set('items_qty',$new_stock);
		$this->db->where('items_id',$items_id);
		$this->db->update('items');	
	}
	
}
