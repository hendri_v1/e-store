<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mselling extends CI_Model {
	
	//sell_out
	var $sell_out_id='';
	var $sell_out_date='';
	var $user_id='';
	var $sell_out_type='';
	var $sell_out_total_dp='';
	var $sell_out_total='';
	var $sell_out_reff='';
	
	//detail_sell_out
	var $detail_sell_out_id='';
	var $items_id='';
	var $detail_sell_out_price='';
	var $qty='';
	var $detail_sell_out_total='';
	
	//customer
	var $customer_id='';
	var $customer_name='';
	var $customer_phone='';
	var $customer_address='';
	
	
	function get_last_id()
	{
		$today=date('dmy');
		$query=$this->db->get('sell_out');
		$total=$query->num_rows;
		$total++;
		$this->db->where('lock_nota_status',0);
		$query=$this->db->get('lock_nota');
		$total_lock=$query->num_rows;
		$total=$total+$total_lock;
		$new_nota=$today.'-'.$total;	
		
		//check unused;
		$this->db->where('lock_nota_status',2);
		$query=$this->db->get('lock_nota');
		$total_unused=$query->num_rows;
		if($total_unused<>0)
		{
			$row=$query->row();
			$new_nota=$row->lock_nota_id;
			$this->db->set('lock_nota_status',0);
			$this->db->where('lock_nota_id',$new_nota);
			$this->db->update('lock_nota');
		}
		else
		{
			$this->db->set('lock_nota_id',$new_nota);
			$this->db->set('lock_nota_user',$this->session->userdata('user_id'));
			$this->db->insert('lock_nota');
		}
		return $new_nota;
	}
	
	function save_sell_out()
	{
		$this->db->where('sell_out_id',$this->sell_out_id);
		$querycheck=$this->db->get('sell_out');
		$total_check=$querycheck->num_rows;
		if($total_check==0)
		{
			$this->db->set('customer_name',$this->customer_name);
			$this->db->set('customer_phone',$this->customer_phone);
			$this->db->set('customer_address',$this->customer_address);
			$this->db->insert('customer');
			$customer_id=$this->db->insert_id();
			
			$this->db->set('sell_out_id',$this->sell_out_id);
			$this->db->set('sell_out_type',$this->sell_out_type);
			$this->db->set('user_id',$this->user_id);
			$this->db->set('sell_out_date',time());
			$this->db->set('sell_out_total',$this->sell_out_total);
			$this->db->set('sell_out_reff',$this->sell_out_reff);
			$this->db->set('customer_id',$customer_id);
			$this->db->insert('sell_out');
		}
		else
		{

			$pisah=explode('-',$this->sell_out_id);
			$new_id=$pisah[1]+1;
			$this->db->set('customer_name',$this->customer_name);
			$this->db->set('customer_phone',$this->customer_phone);
			$this->db->set('customer_address',$this->customer_address);
			$this->db->insert('customer');
			$customer_id=$this->db->insert_id();
			
			$this->db->set('sell_out_id',$new_id);
			$this->db->set('sell_out_type',$this->sell_out_type);
			$this->db->set('user_id',$this->user_id);
			$this->db->set('sell_out_date',time());
			$this->db->set('sell_out_total',$this->sell_out_total);
			$this->db->set('sell_out_reff',$this->sell_out_reff);
			$this->db->set('customer_id',$customer_id);
			$this->db->insert('sell_out');
		}
		
		$this->db->set('lock_nota_status',1);
		$this->db->where('lock_nota_id',$this->sell_out_id);
		$this->db->update('lock_nota');
	}
	
	function save_detail_sell_out()
	{
		$this->db->set('sell_out_id',$this->sell_out_id);
		$this->db->set('items_id',$this->items_id);
		$this->db->set('detail_sell_out_price',$this->detail_sell_out_price);
		$this->db->set('qty',$this->qty);
		$this->db->set('detail_sell_out_total',$this->detail_sell_out_total);
		$this->db->insert('detail_sell_out');
		
		$this->db->where('items_id',$this->items_id);
		$this->db->set('items_status',1);
		$this->db->update('items');
	}
	
	function save_detail_sell_out2($items_qty)
	{
		$this->db->set('sell_out_id',$this->sell_out_id);
		$this->db->set('items_id',$this->items_id);
		$this->db->set('detail_sell_out_price',$this->detail_sell_out_price);
		$this->db->set('qty',$this->qty);
		$this->db->set('detail_sell_out_total',$this->detail_sell_out_total);
		$this->db->insert('detail_sell_out');
		
		$this->db->where('items_id',$this->items_id);
		$query=$this->db->get('items');
		$row=$query->row();
		$new_qty=$row->items_qty-$items_qty;
		
		$this->db->set('items_qty',$new_qty);
		$this->db->where('items_id',$this->items_id);
		$this->db->update('items');
	}
	
	function remove_detail_sell_out($detail_sell_out_id)
	{
		$this->db->where('items_id',$this->items_id);
		$this->db->set('items_status',0);
		$this->db->update('items');
		
		$this->db->where('detail_sell_out_id',$detail_sell_out_id);
		$this->db->delete('detail_sell_out');
	}
	
	function return_detail_sell_out($detail_sell_out_id)
	{
		$this->db->where('items_id',$this->items_id);
		$this->db->set('items_status',2);
		$this->db->update('items');
		
		$this->db->where('detail_sell_out_id',$detail_sell_out_id);
		$this->db->delete('detail_sell_out');
	}
	
	function get_detail_by_id($sell_out_id)
	{
		$this->db->join('items','items.items_id=detail_sell_out.items_id');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->where('detail_sell_out.sell_out_id',$sell_out_id);
		$this->db->order_by('detail_sell_out.detail_sell_out_id','desc');
		$query=$this->db->get('detail_sell_out');
		return $query->result();	
	}
	
	function get_detail_by_items($items_id)
	{
		$this->db->where('items_id',$items_id);
		$query=$this->db->get('detail_sell_out');
		$row=$query->row();
		return $row->detail_sell_out_total;	
	}
	
	function get_detail_by_items_id($items_id)
	{
		$this->db->join('sell_out','sell_out.sell_out_id=detail_sell_out.sell_out_id');
		$this->db->join('user','user.user_id=sell_out.user_id');
		$this->db->where('items_id',$items_id);
		$query=$this->db->get('detail_sell_out');
		return $query->row();
	}
	
	function cancel_sell($sell_out_id)
	{	
		$this->db->where('sell_out_id',$sell_out_id);
		$query=$this->db->get('detail_sell_out');
		//echo $this->db->last_query();
		foreach($query->result() as $rows)
		{
			$this->db->where('items_id',$rows->items_id);
			$this->db->set('items_status',0);
			$this->db->update('items');
			//echo $this->db->last_query();
		}
		
		
		$this->db->where('sell_out_id',$sell_out_id);
		$this->db->delete('detail_sell_out');
		
		$this->db->set('lock_nota_status',2);
		$this->db->where('lock_nota_id',$sell_out_id);
		$this->db->update('lock_nota');	
	}
	
	function get_all_unpaid()
	{
		$this->db->join('user','sell_out.user_id=user.user_id');
		$this->db->where('sell_out.sell_out_status',0);
		$this->db->order_by('sell_out.sell_out_date','desc');
		$query=$this->db->get('sell_out');
		return $query->result();
	}
	
	function change_nota()
	{
		$this->db->order_by('sell_out_date');
		$query=$this->db->get('sell_out');
		$i=0;
		foreach($query->result() as $rows)
		{
			$i++;
			$the_id=substr($rows->sell_out_id,0,7);
			$new_id=$the_id.$i;
			$this->db->where('sell_out_id',$rows->sell_out_id);
			$queryd=$this->db->get('detail_sell_out');
			foreach($queryd->result() as $rowsd)
			{
				$this->db->set('sell_out_id',$new_id);
				$this->db->where('sell_out_id',$rows->sell_out_id);
				$this->db->update('detail_sell_out');	
				echo $this->db->last_query();
			}
			$this->db->set('sell_out_id',$new_id);
			$this->db->where('sell_out_id',$rows->sell_out_id);
			$this->db->update('sell_out');
			echo $this->db->last_query();
		}
	}
	
	function get_sell_out_by_id($sell_out_id)
	{
		$this->db->join('user','user.user_id=sell_out.user_id');
		$this->db->join('customer','customer.customer_id=sell_out.customer_id');
		$this->db->where('sell_out.sell_out_id',$sell_out_id);
		$query=$this->db->get('sell_out');
		return $query->row();
	}
	
	function update_sell_out_by_id($sell_out_id)
	{
		$this->db->set('sell_out_status',1);
		$this->db->where('sell_out_id',$sell_out_id);
		$this->db->update('sell_out');
	}
	
	function get_all_sell_json()
	{
		$kembali=array();
		$query=$this->db->get('sell_out');
		foreach($query->result() as $rows)
		{
			$kembali[]=$rows->sell_out_id;	
		}
		return json_encode($kembali);
		
	}
	
	function generate_report_per_sales($start_date,$end_date)
	{
		$this->db->select('user.user_id');
		$this->db->select('count(user.user_id) as totalnya');
		$this->db->select('staff.staff_name');
		
		$this->db->join('user','user.user_id=sell_out.user_id');
		$this->db->join('staff','staff.user_id=user.user_id');
		$this->db->where('sell_out.sell_out_date >=',$start_date);
		$this->db->where('sell_out.sell_out_date <=',$end_date);
		$this->db->where('sell_out.sell_out_total !=',0);
		$this->db->order_by('user.user_id');
		$this->db->group_by('user.user_id');
		$query=$this->db->get('sell_out');
		return $query->result();
	}
	
	function get_total_profit_by_range($start_date,$end_date,$user_id)
	{
		$this->db->join('sell_out','sell_out.sell_out_id=detail_sell_out.sell_out_id');
		$this->db->join('items','items.items_id=detail_sell_out.items_id');
		$this->db->where('sell_out.sell_out_date >=',$start_date);
		$this->db->where('sell_out.sell_out_date <=',$end_date);
		$this->db->where('sell_out.user_id',$user_id);
		$this->db->where('items.items_unique',0);
		$query=$this->db->get('detail_sell_out');
		$base_price=0;
		$sell_price=0;
		$profit=0;
		$returnnya='';
		foreach($query->result() as $rows)
		{
			if($rows->detail_sell_out_price<>0)
			{
				$base_price=$rows->items_base_price;
				$sell_price=$rows->detail_sell_out_price;
				$profit=$profit+($sell_price-$base_price);
				//$returnnya=$returnnya.$base_price.' vs '.$sell_price.'<br />';
			}
		}
		//return $this->db->last_query();
		return $profit;
		//return $returnnya;
	}

	function per_user_detail_sell($start_date,$end_date,$user_id)
	{
		$this->db->join('sell_out','sell_out.sell_out_id=detail_sell_out.sell_out_id');
		
		$this->db->join('items','items.items_id=detail_sell_out.items_id');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->where('sell_out.sell_out_date >=',$start_date);
		$this->db->where('sell_out.sell_out_date <=',$end_date);
		$this->db->where('sell_out.user_id',$user_id);
		$this->db->where('items.items_unique',0);
		$this->db->where('sell_out.sell_out_total !=',0);
		$query=$this->db->get('detail_sell_out');
		return $query->result();
	}
	
	function get_sell_out_range($start_date,$end_date)
	{
		$this->db->join('sell_out','detail_sell_out.sell_out_id=sell_out.sell_out_id');
		
		$this->db->join('items','items.items_id=detail_sell_out.items_id');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->where('sell_out.sell_out_date >=',$start_date);
		$this->db->where('sell_out.sell_out_date <=',$end_date);
		$query=$this->db->get('detail_sell_out');
		return $query->result();
	}
	
	function get_detail_by_dsell_out_id($detail_sell_out_id)
	{
		$this->db->join('items','items.items_id=detail_sell_out.items_id');
		$this->db->join('product','product.product_id=items.product_id');
		$this->db->join('category','category.category_id=product.category_id');
		$this->db->where('detail_sell_out_id',$detail_sell_out_id);
		$query=$this->db->get('detail_sell_out');
		$total_row=$query->num_rows;
		if($total_row==0)
		{
			
			return 'empty';
		}
		else
		{
			return $query->row();
		}
	}
}

