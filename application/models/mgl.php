<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgl extends CI_Model {
	var $general_ledger_id='';
	var $general_ledger_title='';
	var $general_ledger_type='';
	var $general_ledger_total='';
	var $user_id='';
	var $general_ledger_date='';
	var $general_ledger_balance='';
	var $general_ledger_ref='';
	
	function add_gl()
	{
		$this->db->set('general_ledger_title',$this->general_ledger_title);
		$this->db->set('general_ledger_type',$this->general_ledger_type);
		$this->db->set('general_ledger_total',$this->general_ledger_total);
		$this->db->set('user_id',$this->session->userdata('user_id'));
		$this->db->set('general_ledger_date',time());
		$this->db->set('general_ledger_balance',$this->general_ledger_balance);
		$this->db->set('general_ledger_ref',$this->general_ledger_ref);
		$this->db->insert('general_ledger');	
	}
	
	function get_last_balance()
	{
		$this->db->select('general_ledger_balance');
		$this->db->order_by('general_ledger_id','desc');
		$this->db->limit(1);
		$query=$this->db->get('general_ledger');
		$last_balance=0;
		foreach($query->result() as $rows)
		{
			$last_balance=$last_balance+$rows->general_ledger_balance;
		}
		return $last_balance;
	}
	
	function get_gl_range($start_date,$end_date)
	{
		$this->db->where('general_ledger_date >=',$start_date);
		$this->db->where('general_ledger_date <=',$end_date);
		//$this->db->get('general_ledger');
		$query=$this->db->get('general_ledger');
		return $query->result();	
	}
	
	function get_gl_today($start_date,$end_date)
	{
		$this->db->where('general_ledger_date >=',$start_date);
		$this->db->where('general_ledger_date <=',$end_date);
		$this->db->order_by('general_ledger_id','DESC');
		//$this->db->get('general_ledger');
		$query=$this->db->get('general_ledger');
		return $query->result();	
	}
	
	function update_gl_by_general_ledger_ref($general_ledger_ref)
	{
		$this->db->set('general_ledger_title',$this->general_ledger_title);
		$this->db->where('general_ledger_ref',$general_ledger_ref);
		$this->db->update('general_ledger');
	}
	
	function get_last_gl()
	{
		$this->db->order_by('general_ledger_id','desc');
		$this->db->limit(1);
		$query=$this->db->get('general_ledger');
		$row=$query->row();
		return $row->general_ledger_balance;	
	}
}