<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Muser extends CI_Model {
	
	var $user_id='';
	var $username='';
	var $password='';
	var $last_login='';
	
	var $staff_id='';
	var $staff_name='';
	var $staff_photo='';
	var $staff_status='';
	
	var $staff_position_id='';
	var $staff_position_name='';
	
	function login_check($username,$password)
	{
		//$mpassword=md5($password);
		$this->db->join('staff','staff.user_id=user.user_id');
		$this->db->join('staff_position','staff.staff_position_id=staff_position.staff_position_id');
		$this->db->where('user.username',$username);
		$this->db->where('user.password',$password);
		$querycek=$this->db->get('user');
		$totalcek=$querycek->num_rows;
		//echo $this->db->last_query();
		if($totalcek==0)
			return 0;
		else
		{
			$rowcek=$querycek->row();
			$this->session->set_userdata('user_id',$rowcek->user_id);
			$this->session->set_userdata('staff_id',$rowcek->staff_id);
			$this->session->set_userdata('staff_name',$rowcek->staff_name);
			$this->session->set_userdata('logged','yes');
			$this->session->set_userdata('staff_position_id',$rowcek->staff_position_id);
			$this->session->set_userdata('staff_position_name',$rowcek->staff_position_name);
			return 1;
		}
	}
	
	function get_all()
	{
		$this->db->join('staff','staff.user_id=user.user_id');
		$this->db->join('staff_position','staff_position.staff_position_id=staff.staff_position_id');
		$query=$this->db->get('user');
		return $query->result();		
	}
	
	function get_all_position()
	{
		$query=$this->db->get('staff_position');
		return $query->result();	
	}
	
	function add_new_user()
	{
		$this->db->set('username',$this->username);
		$this->db->set('password',$this->password);
		$this->db->insert('user');
		$index_id=$this->db->insert_id();
		
		$this->db->set('staff_name',$this->staff_name);
		$this->db->set('user_id',$index_id);
		$this->db->set('staff_position_id',$this->staff_position_id);
		$this->db->set('staff_photo','default.jpg');
		$this->db->insert('staff');
			
	}
	
}