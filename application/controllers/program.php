<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Program extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged') == FALSE)
			$this->load->view('login');
		else
			$this->load->view('dashboard');
	}
	
	function login_check()
	{
		$querycek=$this->muser->login_check($this->input->post('username'),md5($this->input->post('password')));
		if($querycek==0)
		{
			$this->session->set_flashdata('error',1);
		}
		else
		{
			
		}
		redirect('program');
	}
	
	function change_my_password()
	{
		$this->load->view('global_include/change_my_pass');	
	}
	
	function change_pass()
	{
		$this->db->set('password',md5($this->input->post('new_password')));
		$this->db->where('user_id',$this->session->userdata('user_id'));
		$this->db->update('user');	
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('program');	
	}
	
	function main_dashboard()
	{
		$query=$this->db->get('global_setting');
		$data['row']=$query->row();
		$this->load->view('global_include/main_dashboard',$data);	
	}
	
	function user_management()
	{
		$this->load->view('admin_include/user_management');	
	}
	
	function global_search()
	{
		
		$data['json_product']=$this->mstock->get_product_for_json();
		$this->load->view('global_include/global_search',$data);	
	}
	
	function backup_db()
	{
		// Load the DB utility class
		$this->load->dbutil();
		
		// Backup your entire database and assign it to a variable
		$backup =& $this->dbutil->backup(); 
		
		// Load the file helper and write the file to your server
		$this->load->helper('file');
		write_file('/path/to/mybackup.gz', $backup); 
		
		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		force_download('mybackup.gz', $backup);
	}
}

