<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cactivity extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('logged') == FALSE)
			redirect('program');
    }
	
	function stock_in($type="hp")
	{
		if(isset($_GET['product_id']))
			$data['dproduct_id']=$_GET['product_id'];
		if(isset($_GET['location_id']))
			$data['dlocation_id']=$_GET['location_id'];
		$data['nothing']='';
		if($type=="modem")
		{
			$data['is_modem']=1;	
		}
		else
			$data['is_modem']=0;
		$kembali=$this->mstock->get_items_for_json();
		$data['the_items_code']=json_encode($kembali['items_code']);
		$data['the_items_name']=json_encode($kembali['items_name']);
		$data['the_items_source']=json_encode($kembali['items_source']);
		$data['the_items_source_ref']=json_encode($kembali['items_source_ref']);
		$this->load->view('wh_include/stock_in',$data);	
	}
	
	function stock_in_acc()
	{
		if(isset($_GET['product_id']))
			$data['dproduct_id']=$_GET['product_id'];
		if(isset($_GET['location_id']))
			$data['dlocation_id']=$_GET['location_id'];
		$data['nothing']='';
		$kembali=$this->mstock->get_items_for_json();
		
		$data['the_items_name']=json_encode($kembali['items_name']);
		$data['the_items_source']=json_encode($kembali['items_source']);
		$data['the_items_source_ref']=json_encode($kembali['items_source_ref']);
		$this->load->view('wh_include/stock_in_acc',$data);	
	}
	
	function save_new_stock()
	{
		$kembali=array();
		$empty=0;
		$error=$this->mstock->check_code_available($this->input->post('items_code'));
		
		/*-------------
		$items_codenya=$this->input->post('items_codeA');
		$kembaliku='';
		foreach($items_codenya as $codenya=>$k)
		{
			$kembaliku=$kembaliku.' - '.$k;
		}
		$kembali['error']=1;
		$kembali['kembaliku']=$kembaliku;
		echo json_encode($kembali);
		-------------- */
		if($this->input->post('items_code')=='')
			$empty=1;
		if($this->input->post('is_second')==2)
			$empty=1;
		if($error==0)
		{
			if($empty==0)
			{
				$this->mstock->items_code=$this->input->post('items_code');
				$this->mstock->items_name=$this->input->post('items_name');
				$this->mstock->items_detail=$this->input->post('items_detail');
				$this->mstock->product_id=$this->input->post('product_id');
				$this->mstock->location_id=$this->input->post('location_id');
				$this->mstock->items_base_price=$this->input->post('items_base_price');
				$this->mstock->items_sell_price=$this->input->post('items_sell_price');
				$this->mstock->items_source=$this->input->post('items_source');
				$this->mstock->items_source_ref=$this->input->post('items_source_ref');
				$this->mstock->items_condition=$this->input->post('items_condition');
				$this->mstock->is_second=$this->input->post('is_second');
				$this->mstock->is_modem=$this->input->post('is_modem');
				$index_id=$this->mstock->add_new_items();
			
				//increase the stock
				$this->mstock->increase_stock($this->input->post('location_id'),1);
			
				//save the log
				$this->mstock->add_activity($this->input->post('location_id'),$index_id,$this->session->userdata('user_id'),1,'Add new Items '.$this->input->post('items_code'));
				if($this->input->post('items_codeA'))
				{
					$emptya=0;
					$items_codenya=$this->input->post('items_codeA');
					foreach($items_codenya as $codenya=>$k)
					{
						if($k=='')
							$emptya=1;
						if($emptya==0)
						{
							$this->mstock->items_code=$k;
							$this->mstock->items_name=$this->input->post('items_name');
							$this->mstock->items_detail=$this->input->post('items_detail');
							$this->mstock->product_id=$this->input->post('product_id');
							$this->mstock->location_id=$this->input->post('location_id');
							$this->mstock->items_base_price=$this->input->post('items_base_price');
							$this->mstock->items_sell_price=$this->input->post('items_sell_price');
							$this->mstock->items_source=$this->input->post('items_source');
							$this->mstock->items_source_ref=$this->input->post('items_source_ref');
							$this->mstock->items_condition=$this->input->post('items_condition');
							$index_id=$this->mstock->add_new_items();
						
							//increase the stock
							$this->mstock->increase_stock($this->input->post('location_id'),1);
						
							//save the log
							$this->mstock->add_activity($this->input->post('location_id'),$index_id,$this->session->userdata('user_id'),1,'Add new Items '.$k);
						}
					}
				}
				$kembali['error']=0;
				$kembali['same_nota']=$this->input->post('same_nota');
				$kembali['same_product']=$this->input->post('same_product');
			}
		}
		else
		{
			$kembali['error']=1;
			$kembali['same_nota']=$this->input->post('same_nota');
			$kembali['same_product']=$this->input->post('same_product');
		}
		echo json_encode($kembali);
		
	}
	
	function save_new_stock2()
	{
		$this->mstock->items_code=$this->input->post('items_code');
		$this->mstock->items_name=$this->input->post('items_name');
		$this->mstock->items_detail=$this->input->post('items_detail');
		$this->mstock->product_id=$this->input->post('product_id');
		$this->mstock->location_id=$this->input->post('location_id');
		$this->mstock->items_base_price=$this->input->post('items_base_price');
		$this->mstock->items_sell_price=$this->input->post('items_sell_price');
		$this->mstock->items_source=$this->input->post('items_source');
		$this->mstock->items_source_ref=$this->input->post('items_source_ref');
		$this->mstock->items_condition=$this->input->post('items_condition');
		$this->mstock->is_second=$this->input->post('is_second');
		$this->mstock->items_qty=$this->input->post('items_qty');
		$index_id=$this->mstock->add_new_items2();
		$kembali['error']=0;
		echo json_encode($kembali);
	}
	
	function update_stock()
	{
		$this->mstock->items_name=$this->input->post('items_name');
		$this->mstock->items_code=$this->input->post('items_code');
		$this->mstock->items_base_price=$this->input->post('items_base_price');
		$this->mstock->items_sell_price=$this->input->post('items_sell_price');
		$this->mstock->items_source=$this->input->post('items_source');
		$this->mstock->items_source_ref=$this->input->post('items_source_ref');
		$this->mstock->product_id=$this->input->post('product_id');
		$this->mstock->is_modem=$this->input->post('is_modem');
		$this->mstock->update_items($this->input->post('items_id'));
		
		$this->mstock->add_activity($this->input->post('location_id'),$this->input->post('items_id'),$this->session->userdata('user_id'),3,'Update Data Items '.$this->input->post('items_code'));
	}
	
	function back_to_stock()
	{
		$this->mstock->change_items_status($this->input->post('items_id'),0);
	}
	
	function replaced()
	{
		$this->mstock->change_items_status($this->input->post('items_id'),4);
	}
}