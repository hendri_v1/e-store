<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuser extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('logged') == FALSE)
			redirect('program');
    }
	
	function index()
	{
		$data['query']=$this->muser->get_all();
		$data['qposition']=$this->muser->get_all_position();
		$this->load->view('admin_include/user_management',$data);
	}
	
	function add_new()
	{
		$this->muser->username=$this->input->post('username');
		$this->muser->password=md5($this->input->post('password'));
		$this->muser->staff_name=$this->input->post('staff_name');
		$this->muser->staff_position_id=$this->input->post('staff_position_id');
		$this->muser->add_new_user();	
	}
	
}