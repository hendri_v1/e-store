<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading"><?php echo $rproduct->product_name;?></div>
			<div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-responsive" id="table-items-in-product">
                        <thead>
                            <tr>
                                <th>#ID</th><th>Status</th><th>Base Price</th><th>Sell Price</th><th>IMEI</th><th>Color</th><th>Sold Price</th><th>Supplier</th><th>Nota</th><th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($query as $rows): ?>
                                <tr <?php if($rows->items_status<>0) echo 'class="terhidden"';?>>
                                    <td><?php echo $rows->items_id;?></td>
                                    <td><?php
										if($rows->items_status==0)
											echo "Available";
										elseif($rows->items_status==1)
											echo "SOLD";
										elseif($rows->items_status==2)
											echo "Just Returned";
										elseif($rows->items_status==3)
											echo "Returned to Sup";
										elseif($rows->items_status==4)
											echo "Replaced";
											?></td>   
                                    <td><?php echo number_format($rows->items_base_price,0,',','.');?></td>
                                    <td><?php echo number_format($rows->items_sell_price,0,',','.');?></td>
                                    <td><?php echo $rows->items_code;?></td>
                                    <td><?php echo $rows->items_name;?></td>
                                   
                                    
                                    <td><?php if($rows->items_status==0)
												echo 0;
											  elseif($rows->items_status==1)
											  	echo number_format($this->mselling->get_detail_by_items($rows->items_id),0,',','.');?></td>
                                    <td><?php echo $rows->supplier_name;?></td> 
                                    <td><?php echo $rows->items_source_ref;?></td>
                                    <td>
                                        <button class="btn btn-primary btn-xs items-detail-btn" data-toggle="modal" data-target="#myModal" items_id="<?php echo $rows->items_id;?>">Detail</button>
                                        
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Details</h4>
      </div>
      <div class="modal-body" id="items-detail-changes">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('.sell_out').click(function(){
			items_id=$(this).val('items_id');
			$('#page-wrapper').load('<?php echo site_url('alluser/cmain/sell_out');?>/'+items_id);
		});
		$('.items-detail-btn').click(function(){
			items_id=$(this).attr('items_id');
			$('#items-detail-changes').load('<?php echo site_url('warehouse/clocation/view_items');?>/'+items_id);
		});
		$('#show-hidden').click(function(){
			$('.terhidden').removeClass('hidden');
		});
		
		$('#table-items-in-product').dataTable({
			"order": [[ 1, "asc" ]]	
		});
    });
</script>