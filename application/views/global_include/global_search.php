<div class="form-group" style="margin-top:5px;">
    <input type="text" class="form-control" placeholder="Search Product" id="global-search-input">
  
        <button class="btn btn-default" type="button" id="search_global_now">
            <i class="fa fa-search"></i>
        </button>
  
</div>
<div class="form-group" style="margin-top:5px;">
    <input type="text" class="form-control" placeholder="Search by Price" id="global-search-price">
    
        <button class="btn btn-default" type="button" id="search_price_now">
            <i class="fa fa-search"></i>
        </button>
   
</div>


<script type="text/javascript">
	$(document).ready(function(e) {
		
		
		
		var json_product = <?php echo $json_product;?>;
		$("#global-search-input").autocomplete(
			{
				source: json_product,
				select: function( event, ui ) {
					var search_key=ui.item;
					if(search_key=='')
						$("#global-search-input").focus();
					else
					{
						$.post('<?php echo site_url('alluser/cmain/search_result');?>',
							{
								keyword:search_key.value
							},
							function(data)
							{
								$('#page-wrapper').html(data);
								$('#global-search-input').val('');
							}
						);
					}	
				}
			}
		);
		
		$('#search_global_now').click(function(){
			val_search=$('#global-search-input').val();
			$.post('<?php echo site_url('alluser/cmain/search_result2');?>',
				{
					keyword:val_search	
				},
				function(data)
				{
					$('#page-wrapper').html(data);
					$('#global-search-input').val('');
				}
			);
		});
		
		$('#global-search-price').keypress(function(e){
			if(e.which == 13) {
				the_price=$(this).val();
				$('#page-wrapper').load('<?php echo site_url('alluser/cmain/search_result3');?>/'+the_price);
			}
		});
		
		$('#search_price_now').click(function(){
			the_price=$('#global-search-price').val();
			$('#page-wrapper').load('<?php echo site_url('alluser/cmain/search_result3');?>/'+the_price);
		});
       
    });
</script>