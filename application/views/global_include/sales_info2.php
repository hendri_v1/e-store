<div class="row">
	<div class="col-lg-6">
        <table class="table">
            <tr><td>Date</td><td><?php echo mdate('%d-%m-%Y %H:%i:%s',$row->sell_out_date);?></td></tr>
            <tr><td>Sales</td>
            	<td>
					<?php echo $row->username;?>
                </td>
            </tr>
            <tr><td>Customer</td><td><?php echo $row->customer_name;?><br /><?php echo $row->customer_address;?><br /><?php echo $row->customer_phone;?></td></tr>
        </table>
    </div>
    <div class="col-lg-6">
    	<table class="table">
        	<tr><td>Nota No</td><td><?php echo $row->sell_out_id;?></td></tr>
        	<tr><td>Payment Type</td><td><?php echo $this->mglobal->sell_out_type($row->sell_out_type);?></td></tr>
            <tr><td>Total</td><td><strong><?php echo number_format($row->sell_out_total,0,',','.');?></strong></td></tr>
        </table>
    </div>
    <div class="col-lg-12">
    	<table class="table table-bordered">
        	<thead>
            	<tr><th>No</th><th>Items</th><th>QTY</th><th>Sub Total</th><th>Action</th></tr>
            </thead>
            <tbody>
            	<?php $i=0; foreach($dquery as $drows): $i++; ?>
                	<tr>
                    	<td><?php echo $i;?></td>
                        <td><?php echo $drows->category_name.' '.$drows->product_name.' '.$drows->items_name;?><br />IMEI <?php echo $drows->items_code;?></td>
                        <td><?php echo $drows->qty;?></td>
                        <td><div align="right"><?php echo number_format($drows->detail_sell_out_price,0,',','.');?></div></td>
                        <td><a href="javascript:void(0);" class="return_this" sell_out_id="<?php echo $row->sell_out_id;?>" items_id="<?php echo $drows->items_id;?>" detail_sell_out_id="<?php echo $drows->detail_sell_out_id;?>">Return</a></td>
                    </tr>
                <?php endforeach;?>
                	
                    	
                
                   
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		$('.return_this').click(function(){
			var r=confirm("Are you sure ?");
			if (r==true)
			{
				d_sell_out_id=$(this).attr('detail_sell_out_id');
				s_id=$(this).attr('sell_out_id');
				i_id=$(this).attr('items_id');
				$.post('<?php echo site_url('alluser/cmain/remove_detail_sell_out2');?>',
				{
					detail_sell_out_id:d_sell_out_id,
					items_id:i_id
				},
				function(data)
				{
					$('#report_result').load('<?php echo site_url('alluser/cmain/sales_info2');?>/'+s_id);
				});
				
			}
			else
			{
				
			}
		});
		
		
    });
</script>
