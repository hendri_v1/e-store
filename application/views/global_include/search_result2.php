<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading"></div>
			<div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-items-in">
                        <thead>
                            <tr>
                                <th>No.</th><th>Merk</th><th>Type</th><th>Spec</th><th>Total</th><th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0; foreach($query as $rows): $i++; ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $rows->category_name;?></td>
                                    <td><?php echo $rows->product_name;?></td>
                                    <td><?php echo str_ireplace($keyword,'<span style="background:red;color:#FFF;">'.$keyword.'</span>',$rows->product_specification);?></td>
                                    <td><?php echo $this->mstock->get_total_by_product($rows->product_id);?></td>
                                    <td>
                                    	
                                    	<button class="btn btn-primary btn-xs items-detail" product_id="<?php echo $rows->product_id;?>" >View All</button>
                                    	
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
	$(document).ready(function(e) {
		$('#table-items-in').dataTable();
		$('#table-items-in .items-detail').click(function(){
			
			the_id=$(this).attr('product_id');
			
		});
    });
</script>