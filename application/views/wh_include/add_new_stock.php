<table class="table">
	<tr><td>Items</td><td><?php echo $row->category_name.' '.$row->product_name.' '.$row->items_name;?></td></tr>
	<tr><td>Last Stock</td><td><?php echo $row->items_qty;?></td></tr>
    <tr><td>New Stock</td><td><input type="text" name="new_stock" id="new_stock" /></td></tr>
    <tr><td colspan="2"><button class="btn btn-info" id="save_add_stock">Add</button></td></tr>
</table>

<script type="text/javascript">
	$(document).ready(function(e) {
        $('#save_add_stock').click(function(){
			$('#myModal').modal('hide');
			i_id='<?php echo $row->items_id;?>';
			i_qty='<?php echo $row->items_qty;?>';
			n_qty=$('#new_stock').val();
			$.post('<?php echo site_url('warehouse/clocation/save_add_stock');?>',
			{
				items_id:i_id,
				items_qty:i_qty,
				new_qty:n_qty
			},
			function(data)
			{
				
				$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in_product_acc/'.$row->product_id.'/1');?>');
			}
			);
		});
    });
</script>