<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Location Management</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-7">
    	<div class="panel panel-default">
        	<div class="panel-heading">All Locations</div>
          	<div class="panel-body">
            	<div class="table-responsive">
                	<table class="table table-striped table-bordered" id="table-location">
                    	<thead>
                        	<tr>
                            	<th>#ID</th><th>Location Name</th><th>Location Info</th><th>Total Items</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php foreach($query as $rows): ?>
                            	<tr>
                                	<td><?php echo $rows->location_id;?></td>
                                    <td><?php echo $rows->location_name;?></td>
                                    <td><?php echo $rows->location_info;?></td>
                                    <td><?php echo $this->mstock->get_total_by_location($rows->location_id);?></td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-5">
    	<div class="panel panel-default">
        	<div class="panel-heading">Add New</div>
            <div class="panel-body">
            	<form role="form" class="form-horizontal">
                	<?php echo $this->mglobal->form_input('location_name','Location Name','text','Input Name');?>
                    <?php echo $this->mglobal->form_input('location_info','Location Info','text','Input Information');?>
                    <button id="add_location" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#table-location').dataTable();
        $('form').on('submit',function(event){
			event.preventDefault();
			var the_data=$(this).serialize();
			$.post('<?php echo site_url('warehouse/clocation/add_new');?>',the_data,function(data){
				$('#page-wrapper').load('<?php echo site_url('warehouse/clocation');?>');
			});
		});
    });
</script>