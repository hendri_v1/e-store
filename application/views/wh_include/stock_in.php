<div class="row">
	<div class="col-lg-12">
       	<h3 class="page-header">Stock In
        <?php if($is_modem==0) echo "Product"; else echo "3rd Party"; ?>
        </h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">Please Complete All Data</div>
            <div class="panel-body">

                <form role="form" class="form-horizontal">
                	<div class="row show" id="step-1-form">
                    	<div class="col-lg-6">
                        	<input type="hidden" name="is_modem" id="is_modem" value="<?php echo $is_modem;?>" />
                        	<?php
								$is_second=array('0'=>"New",'1'=>"Second");
								echo $this->mglobal->form_dropdown('is_second','Status',$is_second);
							?>
                              <?php
                                  $qproduct=$this->mmasterdata->get_all_product();
                                  $product_id=array();
								  $product_id[0]="-- Please Choose --";
                                  foreach($qproduct as $rproduct)
                                  {
                                      $product_id[$rproduct->product_id]=$rproduct->category_name.' - '.$rproduct->product_name;	
                                  }
								?>
                              <?php if(isset($_GET['new_p_id'])): 
								  
                                      if(isset($dproduct_id))
                                        echo $this->mglobal->form_dropdown('product_id','Product Group',$product_id,$dproduct_id);
                                      else	
                                        echo $this->mglobal->form_dropdown('product_id','Product Group',$product_id,$_GET['new_p_id']);
                                  
                                  
                                  echo $this->mglobal->form_input('items_name','Type','text','Type',$_GET['new_i_name']);
                               else: 
                              	 echo $this->mglobal->form_dropdown('product_id','Product Group',$product_id); 
                                echo $this->mglobal->form_input('items_name','Type','text','Type');
                               endif;?>
							  
                              
                              
                             <div class="formgroup">
                             	
                            	<label>
                              	<input type="checkbox" name="same_product" id="same_product"> Use this product for next input
                            	</label>
                          	  </div>
                          </div>
                          <div class="col-lg-6 hidden">
                              <?php echo $this->mglobal->form_textarea('items_detail','Detail Item','Input Detail');?>
                              
                          </div>
                        
                    	<div class="col-lg-6">
                        	<?php
								$qsupplier=$this->mmasterdata->get_all_supplier();
								$items_source=array();
								$items_source[0]="-- Please Choose --";
								foreach($qsupplier as $rsupplier)
								{
									$items_source[$rsupplier->supplier_id]=$rsupplier->supplier_name;	
								}
							?>
                            <?php if(isset($_GET['new_nota'])): ?>
                            	<?php echo $this->mglobal->form_dropdown('items_source','Supplier',$items_source,$_GET['new_nota']); ?>
                            	<?php //echo $this->mglobal->form_input('items_source','Supplier','text','Input Supplier',$_GET['new_supp']);?>
								<?php echo $this->mglobal->form_input('items_source_ref','Source Nota','text','Input Nota',$_GET['new_nota']);?>
                            <?php else: ?>
                            	<?php echo $this->mglobal->form_dropdown('items_source','Supplier',$items_source); ?>
                            	<?php //echo $this->mglobal->form_input('items_source','Supplier','text','Input Supplier');?>
								<?php echo $this->mglobal->form_input('items_source_ref','Source Nota','text','Input Nota');?>                            
                            <?php endif;?>
                            
                            <div class="formgroup">
                            	<label>
                              	<input type="checkbox" name="same_nota" id="same_nota"> Use this nota for next reff
                            	</label>
                          	</div>
                            
                        </div> 
                        <div class="col-lg-12">
                 			<hr />
                        </div>
                        <div class="col-lg-6">
                        	<?php echo $this->mglobal->form_input('items_base_price','Base Price','text','Input Base Price',0);?>
                            <?php echo $this->mglobal->form_input('items_sell_price','Sell Price','text','Input Sell Price',0);?>
                        	<?php
								$qlocation=$this->mstock->get_location();
								$location_id=array();
								foreach($qlocation as $rlocation)
								{
									$location_id[$rlocation->location_id]=$rlocation->location_name.' - '.$rlocation->location_info;	
								}
								echo $this->mglobal->form_dropdown('location_id','Put to Location',$location_id);
							?>
                            <?php echo $this->mglobal->form_textarea('items_condition','Condition Detail','Input Condition');?>
                            <a href="javascript:void(0);" class="btn btn-primary" id="step-2-next">Save</a> <span id="total_items_in_d" class="badge badge-default"></span>
                        </div>
                        <div class="col-lg-6" id="imei_place">
                        	<?php echo $this->mglobal->form_input('items_code','Serial Number','text','Input Serial Number'); ?>
                            
                        </div>
                    </div>
				</form>

                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		var total_items_in_d=0;
		
		var icodetags = <?php echo $the_items_code;?>;
		var inametags = <?php echo $the_items_name;?>;
		
		var isourcereftags = <?php echo $the_items_source_ref;?>;
		$("#items_code").autocomplete({source: icodetags});
		$("#items_name").autocomplete({source: inametags});
		
		$('#items_source_ref').autocomplete({source: isourcereftags});
        $('#step-1-next').click(function(){
			$("#step-1-form").removeClass('show');
			$("#step-2-form").removeClass('hidden');
		 	$('#step-1-form').addClass('hidden');
			$('#step-2-form').addClass('show');
			$('.progress-bar').attr('aria-valuenow','100').attr('style','width:100%;');
		});
		$('#back-to-1').click(function(){
			$("#step-1-form").removeClass('hidden');
			$("#step-2-form").removeClass('show');
		 	$('#step-1-form').addClass('show');
			$('#step-2-form').addClass('hidden');
			$('.progress-bar').attr('aria-valuenow','50').attr('style','width:50%;');
		});
		$('#step-2-next').click(function(){
			var r=confirm("Are you sure you want to save ?");
			if(r==true)
			{
				var the_data=$('form').serialize();
				$.post('<?php echo site_url('warehouse/cactivity/save_new_stock');?>',the_data,function(data){
					if(data.error==0)
					{
						alert('Data Saved');
						if(data.same_nota=='on')
						{
							
							var new_supp=$('#items_source').val();
							var new_nota=$('#items_source_ref').val();
							<?php if(isset($dlocation_id)): ?>
								$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in/'.$dlocation_id);?>?new_nota='+encodeURIComponent(new_nota)'&new_supp='+encodeURIComponent(new_supp));
							<?php else: ?>
								$('#page-wrapper').load('<?php echo site_url('warehouse/cactivity/stock_in');?>?new_nota='+encodeURIComponent(new_nota)+'&new_supp='+encodeURIComponent(new_supp));
							<?php endif;?>
							
						}
						else if(data.same_product=='on')
						{
							var new_prod_id=$('#product_id').val();
							var new_items_name=$('#items_name').val();
							<?php if(isset($dlocation_id)): ?>
								$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in/'.$dlocation_id);?>?new_p_id='+encodeURIComponent(new_prod_id)'&new_i_name='+encodeURIComponent(new_items_name));
							<?php else: ?>
								$('#page-wrapper').load('<?php echo site_url('warehouse/cactivity/stock_in');?>?new_p_id='+encodeURIComponent(new_prod_id)+'&new_i_name='+encodeURIComponent(new_items_name));
							<?php endif;?>
						}
						else
						{
							
							<?php if(isset($dlocation_id)): ?>
								$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in/'.$dlocation_id);?>');
							<?php else: ?>
								$('#page-wrapper').load('<?php echo site_url('warehouse/cactivity/stock_in');?>');
							<?php endif;?>
							
						}
					}
					else
					{
						alert('Item Code is available');
						
					}
				},'json');
			}
		});
		new_imei='<div class="form-group"><label for="items_codeA[]" class="col-sm-2">Serial Number</label><div class="col-sm-10"><input name="items_codeA[]" type="text" class="form-control input-sm added-cols" id="items_codeA[]" placeholder="Input Serial Number" value=""></div></div>';
		$('#add_imei').click(function(){
			//alert(new_imei);
			$('#imei_place').append(new_imei);
		});
		
		$('#items_code').keypress(function(e){
			if(e.which==13)
			{
				$('#imei_place').append(new_imei);
				$('.added-cols').focus();
				total_items_in_d++;
				$('#total_items_in_d').html(total_items_in_d);
			}
		});
		 
		$('#imei_place').on('keypress','.added-cols',function(e){
			if(e.which==13)
			{
				$('#imei_place').append(new_imei);
				$('#imei_place .added-cols').focus();
				total_items_in_d++;
				$('#total_items_in_d').html(total_items_in_d);
			}
		});
		
		
		
    });
</script>