<ol class="breadcrumb">
    	<li><a href="javascript:void(0);">Stock Control System</a></li>
        <li><a href="javascript:void(0);"><?php echo $rlocation->location_name;?></a></li>
        <li><a href="javascript:void(0);" id="product_name_back"><?php echo $rproduct->product_name;?></a></li>
    </ol>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading"><?php echo $rproduct->product_name;?></div>
			<div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-items-in-product">
                        <thead>
                            <tr>
                                <th>#ID</th><th>SN</th><th>Type</th><th>Date In</th><th>Base Price</th><th>Sell Price</th><th>Supplier</th><th>Nota</th><th>Status</th><th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($query as $rows): ?>
                                <tr>
                                    <td><?php echo $rows->items_id;?></td>
                                    <td><?php echo $rows->items_code;?></td>
                                    <td><?php echo $rows->items_name;?></td>
                                    <td><?php echo mdate('%d/%m/%Y %h:%i:%s',$rows->items_date_in);?></td>
                                    <td><?php echo number_format($rows->items_base_price,0,',','.');?></td>
                                    <td><?php echo number_format($rows->items_sell_price,0,',','.');?></td>
                                    <td><?php echo $rows->supplier_name;?></td> 
                                    <td><?php echo $rows->items_source_ref;?></td>
                                    <td><?php echo $this->mglobal->product_status($rows->is_second);?></td>  
                                    <td>
                                        <button class="btn btn-primary btn-xs items-detail-btn" data-toggle="modal" data-target="#myModal" items_id="<?php echo $rows->items_id;?>">Detail</button>
                                        <a href="#" class="btn btn-info btn-xs">Move Location</a>
                                        <a href="#" class="btn btn-warning btn-xs items-edit-btn" data-toggle="modal" data-target="#myModal" items_id="<?php echo $rows->items_id;?>">Edit</a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Details</h4>
      </div>
      <div class="modal-body" id="items-detail-changes">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		
        $('#table-items-in-product .items-detail-btn').click(function(){
			items_id=$(this).attr('items_id');
			
			$('#items-detail-changes').load('<?php echo site_url('warehouse/clocation/view_items');?>/'+items_id);
		});
		
		$('#table-items-in-product .items-edit-btn').click(function(){
			
			items_id=$(this).attr('items_id');
			$('#items-detail-changes').load('<?php echo site_url('warehouse/clocation/edit_items');?>/'+items_id+'?product_id=<?php echo $rproduct->product_id;?>&location_id=<?php echo $rlocation->location_id;?>');
		});
		
		$('#product_name_back').click(function(){
			$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in');?>/'+'<?php echo $rlocation->location_id;?>');
		});
		$('#table-items-in-product').dataTable();
    });
</script>