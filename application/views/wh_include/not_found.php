<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Not Found Stock</h3>
    </div>
</div>
<?php 
	$total_not_found=0;
	$i=0; foreach($query as $rows){ $i++;
	$total_not_found=$i;
}
?>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	<button id="not_found_xls" class="btn btn-primary">Export to XLS </button> <span class="badge pull-right" style="font-size:38px;background:red;"><?php echo $total_not_found;?></span>
            </div>
            <div class="panel-body">
            

                <table class="table table-bordered table-striped">
                    <thead>
                        <th>No</th><th>IMEI</th><th>Condition</th><th>Detail</th>
                    </thead>
                    <tbody>
                        <?php $i=0; foreach($query as $rows): $i++;?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
									<?php
										$panjang=strlen($rows->items_code);
										$akhir=$panjang-4;
										$bintang='';
										for($x=0;$x<=$akhir;$x++)
										{
											$bintang.="*";
										}
										echo $bintang.substr($rows->items_code,$akhir,4);
										
									?>
                                </td>
                                <td>
                                <?php if($rows->is_second==0)
                                        echo "New";
                                    else
                                        echo "Second";
                                ?>
                                </td>
                                <td><?php echo $rows->category_name.' '.$rows->product_name.' '.$rows->items_name;?></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#not_found_xls').click(function(){
	        window.open('<?php echo site_url('warehouse/clocation/not_found_xls/'.$is_second);?>');
		});
    });
</script>