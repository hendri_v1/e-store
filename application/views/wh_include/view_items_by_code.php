<div class="panel panel-default">
	<div class="panel-heading">Source</div>
    <div class="panel-body">
    	<p>
        	Item came from <strong><?php echo $row->items_source;?></strong> with refference number <strong><?php echo $row->items_source_ref;?></strong>
        </p>
    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Item Activity</div>
    <div class="panel-body">
    	<table class="table table-striped">
        	<thead>
            	<tr>
                	<th>No</th><th>Date</th><th>Location</th><th>User</th><th>Activity</th>
                </tr>
            </thead>
            <tbody>
            	<?php $i=0; foreach($qact as $ract): $i++; ?>
                	<tr>
                    	<td><?php echo $i;?></td>
                        <td><?php echo mdate('%d/%m/%Y %h:%i:%s',$ract->stock_activity_date);?></td>
                        <td><?php echo $ract->location_name;?></td>
                        <td><?php echo $ract->username;?></td>
                        <td><?php echo $ract->stock_activity_log;?></td>
                    </tr>
                <?php endforeach;?>
                	<tr>
                    	<td><?php echo $i+1;?></td>
                        <td><?php echo mdate('%d/%m/%Y %h:%i:%s',$rdetail->sell_out_date);?></td>
                        <td></td>
                        <td><?php echo $rdetail->username;?></td>
                        <td>Nota No <?php echo $rdetail->sell_out_id;?></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>