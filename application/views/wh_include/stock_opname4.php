<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Stock Opname HP 2nd</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">Stock Opname Data</div>
            <div class="panel-body">
            	<table class="table">
                	<tr>
                    	<td>Total Items</td><td><strong style="font-size:24px;color:red;"><?php echo $this->mstock->get_all_items("modem");?></strong></td>
                    </tr>
                    <tr>
                        <td>Check Old Data</td><td> <input type="text" id="start_date" name="start_date" placeholder="Start" value="<?php echo date('dmy');?>" /> <button id="show-old-so" class="btn btn-info btn-xs">Show</button></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">Start!</div>
            <div class="panel-body">
            	<?php echo $this->mglobal->form_input('items_code','Input IMEI','text','IMEI');?>
            </div>
        </div>
    </div>
    
    <div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">Result!</div>
            <div class="panel-body" id="search_result">
            	
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#start_date').datepicker(
			{ dateFormat: "ddmmy" }
		);
		
		$("#show-old-so").click(function(){
			the_date=$('#start_date').val();
			$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/not_found_by_date/');?>/'+the_date+'/2/1');
		});
		
		$('#items_code').focus();
        $('#items_code').keypress(function(e){
			if(e.which==13)
			{
				var items_code=$(this).val();
                if($(this).val()!='')
                {
    				$('#search_result').load('<?php echo site_url('warehouse/clocation/stock_o_result');?>/'+items_code+'/2/1');
    				$('#items_code').val('');
    				$('#items_code').focus();
                }
			}
		});
    });
</script>
