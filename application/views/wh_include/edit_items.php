<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">Items Info</div>
            <div class="panel-body">
            	<form role="form" class="form-horizontal">
                	<input type="hidden" id="items_id" name="items_id" value="<?php echo $row->items_id;?>" />
					<?php echo $this->mglobal->form_input('items_code','IMEI','text','Input IMEI',$row->items_code);?>
                    <?php echo $this->mglobal->form_input('items_name','Color','text','Input Color',$row->items_name);?>
                    <?php echo $this->mglobal->form_input('items_base_price','Base Price','text','Input Base Price',$row->items_base_price);?>
                    <?php echo $this->mglobal->form_input('items_sell_price','Sell Price','text','Input Sell Price',$row->items_sell_price);?>
                    <?php echo $this->mglobal->form_input('items_source','Supplier','text','Input Supplier',$row->items_source);?>
                    <?php echo $this->mglobal->form_input('items_source_ref','Nota','text','Input Nota',$row->items_source_ref);?>
                	<?php
						$qproduct=$this->mmasterdata->get_all_product();
						$product_id=array();
						foreach($qproduct as $rproduct)
						{
							$product_id[$rproduct->product_id]=$rproduct->category_name.' - '.$rproduct->product_name;	
						}
						echo $this->mglobal->form_dropdown('product_id','Product Group',$product_id,$row->product_id); 
					?>
                    <?php 
						$is_modem=array(0=>'Handphone',1=>'Modem');
						echo $this->mglobal->form_dropdown('is_modem','Product Type',$is_modem,$row->is_modem);
					?>
                    <button class="btn btn-primary" id="update_items">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
        $('form').on('submit',function(event){
			event.preventDefault();
			var the_data=$('form').serialize();
			$.post('<?php echo site_url('warehouse/cactivity/update_stock');?>',the_data,function(data){
				
				$('#myModal').modal('hide')
				
				alert('success');
				$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in_product/'.$_GET['product_id'].'/'.$_GET['location_id']);?>');
			});
		});
    });
</script>