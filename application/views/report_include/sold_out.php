<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Sold Out Report</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	List of Sold Out
            </div>
            <div class="panel-body" id="report_result">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-items-in">
                        <thead>
                            <tr>
                                <th>No.</th><th>Merk</th><th>Type</th><th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0; foreach($query as $rows): $i++; ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $rows->category_name;?></td>
                                    <td><?php echo $rows->product_name;?></td>
                                    <td><?php echo $this->mstock->get_total_items_by_product($rows->product_id);?></td>
                                    
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table-items-in').dataTable();
    })
</script>