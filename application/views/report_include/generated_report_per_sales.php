<table class="table">
	<thead>
    	<tr>
        	<th>#</th><th>Sales</th><th>Total</th><th>Total Profit</th>
        </tr>
    </thead>
    <tbody>
    	<?php
			$user_id=array(); 
			$total_profit=0;
			$i=0; foreach($query as $rows): $i++; 
			$user_id[$i]=$rows->user_id;
			$profit_in=$this->mselling->get_total_profit_by_range($start_date,$end_date,$rows->user_id);
			$total_profit=$total_profit+$profit_in;
			?>
        	<tr>
            	<td></td>
                <td><?php echo $rows->staff_name;?></td>
                <td><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="detail_sales" start-date="<?php echo $start_date;?>" end-date="<?php echo $end_date;?>" user-id="<?php echo $rows->user_id;?>"><?php echo $rows->totalnya;?></a></td>
                <td><div align="right"><?php //echo $this->mselling->get_total_profit_by_range($start_date,$end_date,$rows->user_id);
				echo number_format($profit_in,0,',','.');?></div></td>
            </tr>
        <?php endforeach;?>
        	<tr><td colspan="3"><div align="right">Total</div></td><td><div align="right"><strong><?php echo number_format($total_profit,0,',','.');?></strong></div></td></tr>
    </tbody>
</table>

<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Sell Detail</h4>
      </div>
      <div class="modal-body" id="unpaid-sales-change">
        
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('.detail_sales').click(function(){
            s_date=$(this).attr('start-date');
            e_date=$(this).attr('end-date');
            u_id=$(this).attr('user-id');
            $.post('<?php echo site_url('super_admin/creport/per_sales_detail');?>',
            {
                start_date:s_date,
                end_date:e_date,
                user_id:u_id
            },
            function(data)
            {

                $('#unpaid-sales-change').html(data);
            });
        })
    })
</script>