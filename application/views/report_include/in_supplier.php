<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">In Supplier List</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	
            </div>
            <div class="panel-body" id="report_result">
            	<table class="table table-bordered" id="table-retsup">
                	<thead>
                    	<tr>
                        	<th>#</th><th>Product Name</th><th>IMEI</th><th>Supplier</th><th>Source</th><th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php $i=0; foreach($query as $rows): $i++; ?>
                        	<tr>
                            	<td><?php echo $i;?></td>
                                <td><?php echo $rows->product_name.' '.$rows->items_name;?></td>
                                <td><?php echo $rows->items_code;?></td>
                                <td><?php echo $rows->supplier_name;?></td>
                                <td><?php echo $rows->items_source_ref;?></td>
                                <td><a href="javascript:void(0);" class="btn btn-info btn-xs bts" data-iid="<?php echo $rows->items_id;?>">Back to Stock</a> <a href="javascript:void(0);" class="btn btn-success btn-xs rps" data-iid="<?php echo $rows->items_id;?>">Replaced</a></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
        $('.bts').click(function(){
			var r=confirm("Are you sure ?");
			if (r==true)
			{
				i_id=$(this).data('iid');
				$.post('<?php echo site_url('warehouse/cactivity/back_to_stock');?>',
				{
					items_id:i_id
				},
				function(data)
				{
					$('#page-wrapper').load('<?php echo site_url('super_admin/creport/in_supplier');?>');
				});
			}
		});
		
		$('.rps').click(function(){
			var r=confirm("Are you sure ?");
			if (r==true)
			{
				i_id=$(this).data('iid');
				$.post('<?php echo site_url('warehouse/cactivity/replaced');?>',
				{
					items_id:i_id
				},
				function(data)
				{
					$('#page-wrapper').load('<?php echo site_url('warehouse/cactivity/stock_in');?>');
				});
			}
		});
		
		$('#table-retsup').dataTable();
		
    });
</script>