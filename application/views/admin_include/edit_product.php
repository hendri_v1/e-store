<div class="panel panel-default">
        	<div class="panel-heading">New Product</div>
            <div class="panel-body">
            	<form role="form" class="form-horizontal">
                	<input type="hidden" name="product_id" value="<?php echo $row->product_id;?>" id="product_id">
                	<?php echo $this->mglobal->form_input('product_name','Product Name','text','Enter Name',$row->product_name);?>
                    <?php echo $this->mglobal->form_input('product_information','Product Information','text','Enter Information',$row->product_information);?>
                    <div class="form-group">
                        <label for="category_id" class="col-sm-2">Category</label>
                        <div class="col-sm-10">
                        <?php
                            $category_id=array();
                            foreach($qcategory as $rowscategory)
                            {
                                $category_id[$rowscategory->category_id]=$rowscategory->category_name;	
                            }
                            echo form_dropdown('category_id',$category_id,$row->category_id,'id="category_id" class="form-control"');
                            ?>
                            
                     </div>
                     </div>
                     <?php echo $this->mglobal->form_textarea('product_specification','Product Specification','Input Spec',$row->product_specification);?>
                        <button class="btn btn-primary" id="edit_product"><i class="fa fa-check"></i> Save</button>
					                    
                </form>
            </div>
        </div>
        
    <script type="application/javascript">
		$(document).ready(function(){
			$('form').on('submit',function(event){
				event.preventDefault();
				var the_data=$(this).serialize();
				$.post('<?php echo site_url('super_admin/cproduct/edit_now');?>',the_data,function(data){
					$('#page-wrapper').load('<?php echo site_url('super_admin/cproduct');?>');
				});
			});
		});
	</script>